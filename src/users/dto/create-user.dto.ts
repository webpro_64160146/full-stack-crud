export class CreateUserDto {
  name: string;
  age: number;
  tel: string;
  gender: string;
}
